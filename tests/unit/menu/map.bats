load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/map.sh"

setup() {
    # shellcheck disable=SC1091
    . shellmock
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "map:action:field returns proper exit code" {
    # GIVEN
    ICON=
    NAME=pass
    THEME=magenta
    KEY=P
    BINDINGS=%-kb-custom-2%Ctrl-P
    DEFAULT_ACTION=COPY
    INTERPRETER=
    SEPARATOR=
    LINES_COUNT=1
    MULTI=false
    ACTIONS_LIST=12/EDIT/pink/3,13/OPEN/red/1
    ACTIONS_BINDINGS=%-kb-custom-3%Ctrl-E
    EXIT_CODE=11
    SOURCE="$ICON:$NAME:$THEME:$KEY:$BINDINGS:$DEFAULT_ACTION:$INTERPRETER:$SEPARATOR:$LINES_COUNT:$MULTI:$ACTIONS_LIST:$ACTIONS_BINDINGS:$EXIT_CODE"

    ACTION=OPEN

    # WHEN
    run map:action:field "$ACTION" "$SOURCE" exit_code

    # THEN
    assert_success
    assert_output 13
}

@test "map:action:field returns proper name" {
    # GIVEN
    ICON=
    NAME=pass
    THEME=magenta
    KEY=P
    BINDINGS=%-kb-custom-2%Ctrl-P
    DEFAULT_ACTION=COPY
    INTERPRETER=
    SEPARATOR=
    LINES_COUNT=1
    MULTI=false
    ACTIONS_LIST=12/EDIT/pink/3,13/OPEN/red/1
    ACTIONS_BINDINGS=%-kb-custom-3%Ctrl-E
    EXIT_CODE=11
    SOURCE="$ICON:$NAME:$THEME:$KEY:$BINDINGS:$DEFAULT_ACTION:$INTERPRETER:$SEPARATOR:$LINES_COUNT:$MULTI:$ACTIONS_LIST:$ACTIONS_BINDINGS:$EXIT_CODE"


    ACTION=OPEN

    # WHEN
    run map:action:field "$ACTION" "$SOURCE" name

    # THEN
    assert_success
    assert_output OPEN
}

@test "map:action:field returns proper theme" {
    # GIVEN
    ICON=
    NAME=pass
    THEME=magenta
    KEY=P
    BINDINGS=%-kb-custom-2%Ctrl-P
    DEFAULT_ACTION=COPY
    INTERPRETER=
    SEPARATOR=
    LINES_COUNT=1
    MULTI=false
    ACTIONS_LIST=12/EDIT/pink/3,13/OPEN/red/1
    ACTIONS_BINDINGS=%-kb-custom-3%Ctrl-E
    EXIT_CODE=11
    SOURCE="$ICON:$NAME:$THEME:$KEY:$BINDINGS:$DEFAULT_ACTION:$INTERPRETER:$SEPARATOR:$LINES_COUNT:$MULTI:$ACTIONS_LIST:$ACTIONS_BINDINGS:$EXIT_CODE"

    ACTION=OPEN

    # WHEN
    run map:action:field "$ACTION" "$SOURCE" theme

    # THEN
    assert_success
    assert_output red
}

@test "map:action:field returns proper number of lines" {
    # GIVEN
    ICON=
    NAME=pass
    THEME=magenta
    KEY=P
    BINDINGS=%-kb-custom-2%Ctrl-P
    DEFAULT_ACTION=COPY
    INTERPRETER=
    SEPARATOR=
    LINES_COUNT=1
    MULTI=false
    ACTIONS_LIST=12/EDIT/pink/3,13/OPEN/red/1
    ACTIONS_BINDINGS=%-kb-custom-3%Ctrl-E
    EXIT_CODE=11
    SOURCE="$ICON:$NAME:$THEME:$KEY:$BINDINGS:$DEFAULT_ACTION:$INTERPRETER:$SEPARATOR:$LINES_COUNT:$MULTI:$ACTIONS_LIST:$ACTIONS_BINDINGS:$EXIT_CODE"

    ACTION=EDIT

    # WHEN
    run map:action:field "$ACTION" "$SOURCE" lines

    # THEN
    assert_success
    assert_output 3
}
