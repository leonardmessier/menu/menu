load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/show/rofi_args.sh"

setup() {
    # shellcheck disable=SC1091
    . shellmock
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "show:rofi:args builds default command line argument" {
    # GIVEN
    ACTION=
    THEME_FILE='green.rasi'
    #       1 2                 3     4 5                    6   78 9 10   111213
    SOURCE='x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:::1:false:-::10'

    # WHEN
    run show:rofi:args "$THEME_FILE" "$SOURCE" "$ACTION"

    # THEN
    assert_success
    assert_output '-markup-rows -dmenu -p x -theme green.rasi -kb-custom-1 Ctrl-B'
}
