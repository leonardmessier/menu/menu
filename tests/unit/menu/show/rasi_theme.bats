load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/show/module.sh"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -d "$HOME/.cache/menu/rasi" ]; then
        rm -rf "$HOME/.cache/menu/rasi"
    fi

    if [ -d "$MENU_LOCAL_SHARE/themes/books" ]; then
        rm -rf "$MENU_LOCAL_SHARE/themes/books"
    fi
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "show:rasi:theme returns built-in theme name" {
    # GIVEN
    local THEME=green
    local SCRIPT=books

    # WHEN
    run show:rasi:theme "$THEME" "$SCRIPT"

    assert_success
    assert_output "/code/share/themes/rofi/show/colorschemes/green.rasi"
}

@test "show:rasi:theme returns user script theme if it exists" {
    # GIVEN
    local THEME=green
    local SCRIPT=books

    mkdir -p "$MENU_LOCAL_SHARE/themes/books"
    touch "$MENU_LOCAL_SHARE/themes/books/green.rasi"

    run show:rasi:theme "$THEME" "$SCRIPT"

    assert_success
    assert_output "$MENU_LOCAL_SHARE/themes/books/green.rasi"
}

@test "show:rasi:theme fails with 1 if theme does not exist" {
    # GIVEN
    local THEME=crazy
    local SCRIPT=books

    # WHEN
    run show:rasi:theme "$THEME" "$SCRIPT"

    assert_failure 1
}

