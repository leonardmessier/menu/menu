load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/show/module.sh"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -d "$HOME/.cache/menu/rasi" ]; then
        rm -rf "$HOME/.cache/menu/rasi"
    fi

    if [ -d "$MENU_LOCAL_SHARE/themes/books" ]; then
        rm -rf "$MENU_LOCAL_SHARE/themes/books"
    fi
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "show:rasi:layout returns built-in layout if it exists" {
    # GIVEN
    local LAYOUT=list
    local SCRIPT=books

    # WHEN
    run show:rasi:layout "$LAYOUT"

    assert_success
    assert_output "/code/share/themes/rofi/show/layouts/list.rasi"
}
