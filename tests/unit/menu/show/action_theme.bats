#!/usr/bin/env bash
load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/show/module.sh"

setup() {
    # shellcheck disable=SC1091
    . shellmock
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "show:action:theme outputs main theme if action is not set" {
    # GIVEN
    METADATA=':pass:magenta:P:%-kb-custom-2%Ctrl-P:COPY::::12/EDIT/pink/default/1:%-kb-custom-3%Ctrl-E:11'
    ACTION=

    # WHEN
    run show:action:theme "$METADATA" "$ACTION"

    # THEN
    assert_success
    assert_output magenta
}

@test "show:action:theme outputs main theme if action is the default one" {
    # GIVEN
    METADATA=':pass:magenta:P:%-kb-custom-2%Ctrl-P:COPY::::12/EDIT/pink/default/1:%-kb-custom-3%Ctrl-E:11'
    ACTION=COPY

    # WHEN
    run show:action:theme "$METADATA" "$ACTION"

    # THEN
    assert_success
    assert_output magenta
}

@test "show:action:theme outputs action theme if the action is not the default one" {
    # GIVEN
    ICON=
    NAME=pass
    THEME=magenta
    KEY=P
    BINDINGS=%-kb-custom-2%Ctrl-P
    DEFAULT_ACTION=COPY
    INTERPRETER=
    SEPARATOR=
    LINES_COUNT=1
    MULTI=false
    ACTIONS_LIST=12/EDIT/pink/default/1
    ACTIONS_BINDINGS=%-kb-custom-3%Ctrl-E
    EXIT_CODE=11
    METADATA="$ICON:$NAME:$THEME:$KEY:$BINDINGS:$DEFAULT_ACTION:$INTERPRETER:$SEPARATOR:$LINES_COUNT:$MULTI:$ACTIONS_LIST:$ACTIONS_BINDINGS:$EXIT_CODE"
    ACTION=EDIT

    # WHEN
    run show:action:theme "$METADATA" "$ACTION"

    # THEN
    assert_success
    assert_output pink
}





