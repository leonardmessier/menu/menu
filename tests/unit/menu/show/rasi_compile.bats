load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/show/module.sh"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -d "$HOME/.cache/menu/rasi" ]; then
        rm -rf "$HOME/.cache/menu/rasi"
    fi

    if [ -d "$MENU_LOCAL_SHARE/themes/books" ]; then
        rm -rf "$MENU_LOCAL_SHARE/themes/books"
    fi
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "show:rasi:compile returns the rasi file path if it exists" {
    # GIVEN
    local THEME=golden
    local LAYOUT=crazy
    local SCRIPT=books

    mkdir -p "$HOME/.cache/menu/rasi"
    touch "$HOME/.cache/menu/rasi/golden-crazy.rasi"

    # WHEN
    run show:rasi:compile "$THEME" "$LAYOUT" "$SCRIPT"

    # THEN
    assert_success
    assert_output "$HOME/.cache/menu/rasi/golden-crazy.rasi"
}

@test "show:rasi:compile creates the rasi file if it does not exist and return the path" {
    # GIVEN
    local THEME=green
    local LAYOUT=list
    local SCRIPT=books

    # WHEN
    run show:rasi:compile "$THEME" "$LAYOUT" "$SCRIPT"

    # THEN
    assert_success
    assert_file_exists "$HOME/.cache/menu/rasi/green-list.rasi"
    assert_output "$HOME/.cache/menu/rasi/green-list.rasi"
}

@test "show:rasi:compile errors out with 1 if theme does not exist" {
    # GIVEN
    local THEME=crazy
    local LAYOUT=default
    local SCRIPT=books

    # WHEN
    run show:rasi:compile "$THEME" "$LAYOUT" "$SCRIPT"

    # THEN
    assert_failure 1
}

@test "show:rasi:compile errors out with 1 if layout does not exist" {
    # GIVEN
    local THEME=green
    local LAYOUT=any
    local SCRIPT=books

    # WHEN
    run show:rasi:compile "$THEME" "$LAYOUT" "$SCRIPT"

    # THEN
    assert_failure 2
}


