load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/functions"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -f "${MENU_SCRIPTS_DIR}/script-no-icon" ]; then
        rm "${MENU_SCRIPTS_DIR}/script-no-icon"
    fi

    if [ -f "${MENU_SCRIPTS_DIR}/script-green-open" ]; then
        rm "${MENU_SCRIPTS_DIR}/script-green-open"
    fi

    if [ -f "${MENU_SCRIPTS_DIR}/script-red-browse" ]; then
        rm "${MENU_SCRIPTS_DIR}/script-red-browse"
    fi

}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}



