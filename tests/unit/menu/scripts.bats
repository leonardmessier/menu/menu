load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"

load "$_MENU_ROOT/tests/unit/helpers/assert"

load "$_MENU_ROOT/share/menu/scripts.sh"

setup() {
    # shellcheck disable=SC1091
    . shellmock

    if [ -f "${MENU_SCRIPTS_DIR}/script-no-icon" ]; then
        rm "${MENU_SCRIPTS_DIR}/script-no-icon"
    fi

    if [ -f "${MENU_SCRIPTS_DIR}/script-green-open" ]; then
        rm "${MENU_SCRIPTS_DIR}/script-green-open"
    fi

    if [ -f "${MENU_SCRIPTS_DIR}/script-red-browse" ]; then
        rm "${MENU_SCRIPTS_DIR}/script-red-browse"
    fi

}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi
}

@test "scripts:read ignores scripts without icons" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN

EOF
    )" > "$MENU_SCRIPTS_DIR/script-no-icon"

    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output ""
}

@test "scripts:read has a default lines count of 1 and a default layout of script" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:x
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN

EOF
    )" > "$MENU_SCRIPTS_DIR/script-green-open"

    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output 'x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:::1:false:-::10'
}

@test "scripts:read sets the given interpreter" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:x
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN
# INTERPRETER:php

EOF
    )" > "$MENU_SCRIPTS_DIR/script-green-open"

    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output 'x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:php::1:false:-::10'
}

@test "scripts:read sets the given layout" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:x
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN

EOF
    )" > "$MENU_SCRIPTS_DIR/script-green-open"

    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output 'x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:::1:false:-::10'
}

@test "scripts:read works with several files" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:x
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN
# MAIN_LAYOUT:grid

EOF
    )" > "$MENU_SCRIPTS_DIR/script-green-open"

    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:µ
# THEME:red
# MAIN_KEY:C
# DEFAULT_ACTION:BROWSE

EOF
    )" > "$MENU_SCRIPTS_DIR/script-red-browse"


    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:::1:false:-::10
µ:script-red-browse:red:C:%-kb-custom-2%Ctrl-C:BROWSE:::1:false:-::11
EOF
    )"
}

@test "scripts:read allows secondary actions to be defined inheriting certain properties from the main script" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:x
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN
# ACTION:SELECT-TAGS/Alt-S////

EOF
    )" > "$MENU_SCRIPTS_DIR/script-green-open"

    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output 'x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:::1:false:11/SELECT-TAGS/green/1/false:%-kb-custom-2%Alt-S:10'
}

@test "scripts:read allows secondary actions to be define their own layout, theme, number of lines and multi select" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:x
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN
# ACTION:SELECT-TAGS/Alt-S/teal/3/true

EOF
    )" > "$MENU_SCRIPTS_DIR/script-green-open"

    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output 'x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:::1:false:11/SELECT-TAGS/teal/3/true:%-kb-custom-2%Alt-S:10'
}

@test "scripts:read allows several scripts and secondary actions" {
    # GIVEN
    echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:x
# THEME:green
# MAIN_KEY:B
# DEFAULT_ACTION:OPEN
# MAIN_LAYOUT:grid
# ACTION:SELECT-TAGS/Alt-S/teal/3/true
# ACTION:SEARCH-TAGS/Alt-E/turquoise/1/false

EOF
    )" > "$MENU_SCRIPTS_DIR/script-green-open"

  echo "$(cat <<-EOF
#!/usr/bin/env bash

# PROMPT:µ
# THEME:red
# MAIN_KEY:C
# DEFAULT_ACTION:BROWSE
# ACTION:OPEN/Alt-O/orange/1/false

EOF
    )" > "$MENU_SCRIPTS_DIR/script-red-browse"

    # WHEN
    run scripts:read

    # THEN
    assert_success
    assert_output "$(cat <<-EOF
x:script-green-open:green:B:%-kb-custom-1%Ctrl-B:OPEN:::1:false:11/SELECT-TAGS/teal/3/true,12/SEARCH-TAGS/turquoise/1/false:%-kb-custom-2%Alt-S%-kb-custom-3%Alt-E:10
µ:script-red-browse:red:C:%-kb-custom-4%Ctrl-C:BROWSE:::1:false:14/OPEN/orange/1/false:%-kb-custom-5%Alt-O:13
EOF
    )"
}

