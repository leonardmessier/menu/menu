if [[ ! -o interactive ]]; then
    return
fi

compctl -K _menu menu

_menu() {
  local word words completions
  read -cA words
  word="${words[2]}"

  if [ "${#words}" -eq 2 ]; then
    completions="$(menu commands)"
  else
    completions="$(menu completions "${word}")"
  fi

  reply=("${(ps:\n:)completions}")
}
