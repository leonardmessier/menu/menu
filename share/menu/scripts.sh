#!/bin/bash

scripts:read() {
    local NAME
    local THEME
    local ICON
    local SCRIPTS_MAP=''
    local i=1
    local EXIT_CODE
    local BINDINGS
    local MULTI

    local SCRIPTS_DIR
    if [ -z "$MENU_SCRIPTS_DIR" ]; then
        SCRIPTS_DIR="$HOME/.local/share/menu/scripts/*"
    else
        SCRIPTS_DIR="$MENU_SCRIPTS_DIR/*"
    fi

    SCRIPTS_MAP=''
    for SCRIPT in $SCRIPTS_DIR; do
        if ! READ_ONE="$(scripts:read:one "$SCRIPT" "$i")"; then
            continue
        fi

        if [ "$SCRIPTS_MAP" != '' ]; then
            SCRIPTS_MAP+="\n"
        fi
        SCRIPT_DEF=$(echo -e "$READ_ONE" | cut -d$'\n' -f1)
        i=$(echo -e "$READ_ONE" | cut -d$'\n' -f2)

        SCRIPTS_MAP+=$SCRIPT_DEF

        i=$(( i + 1 ))
    done

    echo -e "$SCRIPTS_MAP"
}

scripts:read:one() {
    local SCRIPT=$1
    local INDEX=${2:-1}

    ICON=$(grep 'PROMPT:' "$SCRIPT" | cut -d: -f 2)
    if [ -z "$ICON" ]; then
        return 1
    fi
    THEME=$(grep 'THEME:' "$SCRIPT" | cut -d: -f 2)

    NAME=$(basename "$SCRIPT")

    KEY=$(grep 'MAIN_KEY:' "$SCRIPT" | cut -d: -f 2)
    BINDINGS="%-kb-custom-$INDEX%Ctrl-$KEY"
    DEFAULT_ACTION=$(grep 'DEFAULT_ACTION:' "$SCRIPT" | cut -d: -f 2)
    SEPARATOR=$(grep '# SEPARATOR:' "$SCRIPT" | cut -d: -f 2)

    LINES_COUNT=$(grep '# LINES:' "$SCRIPT" | cut -d: -f 2)
    if [ -z "$LINES_COUNT" ]; then
        LINES_COUNT=1
    fi
    echo "LINES_COUNT: $LINES_COUNT" >> "$MENU_LOG_FILE"

    INTERPRETER=$(grep '# INTERPRETER:' "$SCRIPT" | cut -d: -f 2)
    echo "INTERPRETER: $INTERPRETER" >> "$MENU_LOG_FILE"

    MULTI=$(grep '# MULTI:' "$SCRIPT" | cut -d: -f 2)
    if [ -z "$MULTI" ]; then
        MULTI=false
    fi

    EXIT_CODE=$(( INDEX + 9 ))

    local ACTIONS
    ACTIONS=$(grep '# ACTION:' "$SCRIPT" | cut -d: -f 2)

    local ACTION_NAME=
    local ACTION_KEY=
    local ACTION_THEME=
    local ACTION_LINES=
    local ACTION_EXIT_CODE=
    local ACTION_MULTI=
    local ACTIONS_BINDINGS=''
    local ACTIONS_LIST=''
    for ACTION in $ACTIONS; do
        if [ "$ACTIONS_LIST" != "" ]; then
            ACTIONS_LIST+=","
        fi

        ACTION_NAME=$(echo "$ACTION" | cut -d/ -f1)
        ACTION_KEY=$(echo "$ACTION" | cut -d/ -f2)
        ACTION_THEME=$(echo "$ACTION" | cut -d/ -f3)
        if [ -z "$ACTION_THEME" ]; then
            ACTION_THEME=$THEME
        fi

        ACTION_LINES=$(echo "$ACTION" | cut -d/ -f4)
        if [ -z "$ACTION_LINES" ]; then
            ACTION_LINES=$LINES_COUNT
        fi

        if [ -n "$ACTION_KEY" ]; then
            INDEX=$(( INDEX + 1 ))
            ACTION_EXIT_CODE=$(( INDEX + 9 ))
            ACTIONS_BINDINGS+="%-kb-custom-$INDEX%$ACTION_KEY"

        fi
        ACTION_MULTI=$(echo "$ACTION" | cut -d/ -f5)
        if [ -z "$ACTION_MULTI" ]; then
            ACTION_MULTI=$MULTI
        fi

        ACTIONS_LIST+="$ACTION_EXIT_CODE/$ACTION_NAME/$ACTION_THEME/$ACTION_LINES/$ACTION_MULTI"
    done

    if [ "$ACTIONS_LIST" == '' ]; then
        ACTIONS_LIST="-"
    fi
    #        1     2     3      4    5         6               7            8          9            10     11            12                13          UPDATED COUNTER
    echo -e "$ICON:$NAME:$THEME:$KEY:$BINDINGS:$DEFAULT_ACTION:$INTERPRETER:$SEPARATOR:$LINES_COUNT:$MULTI:$ACTIONS_LIST:$ACTIONS_BINDINGS:$EXIT_CODE\n$INDEX"
}
