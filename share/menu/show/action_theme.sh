#!/usr/bin/env bash

# shellcheck disable=SC1090
source "$_MENU_ROOT/share/menu/map.sh"

# Outputs either the main theme declared as `THEME` property in the
# extension or the specific action theme declared in the action
# definition
show:action:theme() {
    local METADATA=$1
    local ACTION=$2

    if [ -z "$ACTION" ] || _is_default_action "$ACTION" "$METADATA"; then
        map:field "$METADATA" theme
        return 0
    fi

    map:action:field "$ACTION" "$METADATA" theme
}

_is_default_action() {
    local ACTION=$1
    local METADATA=$2

    local DEFAULT_ACTION
    DEFAULT_ACTION=$(map:field "$METADATA" 'default_action')

    if [ "$DEFAULT_ACTION" != "$ACTION" ]; then
        return 1
    fi

    return 0
}
