#!/usr/bin/env bash

show:rasi:compile() {
    local THEME=$1
    local LAYOUT=$2
    local SCRIPT=$3

    declare -ir ERROR_MISSING_THEME=1
    declare -ir ERROR_MISSING_LAYOUT=2

    local BASE_CACHE_DIR="$HOME/.cache/menu/rasi"
    mkdir -p "$BASE_CACHE_DIR"

    RASI_FILE_PATH="$BASE_CACHE_DIR/$THEME-$LAYOUT.rasi"
    if [ -f "$RASI_FILE_PATH" ]; then
        echo "$BASE_CACHE_DIR/$THEME-$LAYOUT.rasi"
        return 0
    fi

    if ! THEME_FILE_PATH="$(show:rasi:theme "$THEME" "$SCRIPT")"; then
        echo "No theme file found" >> "$MENU_LOG_FILE"
        return $ERROR_MISSING_THEME
    fi
    echo "THEME_FILE_PATH: $THEME_FILE_PATH" >> "$MENU_LOG_FILE"

    if ! LAYOUT_FILE_PATH="$(show:rasi:layout "$LAYOUT")"; then
        echo "No layout file found" >> "$MENU_LOG_FILE"
        return $ERROR_MISSING_LAYOUT
    fi

    echo "LAYOUT_FILE_PATH: $LAYOUT_FILE_PATH" >> "$MENU_LOG_FILE"

    local BASE_RASI_SHOW_PATH="$_MENU_ROOT/share/themes/rofi/show"
    echo "default rasi file: $BASE_RASI_SHOW_PATH/default.rasi" >> "$MENU_LOG_FILE"
    echo "colors file: $BASE_RASI_SHOW_PATH/colors.rasi" >> "$MENU_LOG_FILE"

    cat \
        "$BASE_RASI_SHOW_PATH/colors.rasi" \
        "$LAYOUT_FILE_PATH" \
        "$THEME_FILE_PATH" \
        "$BASE_RASI_SHOW_PATH/default.rasi" \
    > "$RASI_FILE_PATH"
    echo "cat outcome: $?" >> "$MENU_LOG_FILE"

    echo "$RASI_FILE_PATH"
    return 0
}
