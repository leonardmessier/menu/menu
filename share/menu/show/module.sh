#!/usr/bin/env bash

# shellcheck disable=SC1090
source "$_MENU_ROOT/share/menu/show/rasi_compile.sh"
source "$_MENU_ROOT/share/menu/show/rasi_theme.sh"
source "$_MENU_ROOT/share/menu/show/rasi_layout.sh"
source "$_MENU_ROOT/share/menu/show/rofi_args.sh"
source "$_MENU_ROOT/share/menu/show/action_theme.sh"
