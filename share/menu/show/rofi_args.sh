#!/bin/bash

# shellcheck disable=SC1090
source "$_MENU_ROOT/share/menu/map.sh"

show:rofi:args() {
    local theme_file=$1
    local source=$2
    local action=$3

    local icon
    icon=$(map:field "$source" icon)

    local rofi_args="-markup-rows -dmenu -p $icon -theme $theme_file"

    local lines_count
    if [ -z "$action" ]; then
        lines_count=$(map:field "$source" lines_count)
    else
        lines_count=$(map:action:field "$action" "$source" lines)
    fi
    if [ -n "$lines_count" ] && [ "$lines_count" -gt 1 ]; then
        rofi_args+=" -eh $lines_count"
    fi

    local bindings
    bindings=$(map:field "$source" binding | tr -d '\n' | tr '%' ' ')

    local action_bindings
    action_bindings=$(map:field "$source" actions_bindings | tr -d '\n' | tr '%' ' ')
    if [ -n "$action_bindings" ]; then
        bindings+="$action_bindings"
    fi

    if [ -n "$bindings" ]; then
        rofi_args+=$bindings
    fi

    local separator
    separator=$(map:field "$source" separator)
    if [ -n "$separator" ]; then
        rofi_args+=" -sep $separator"
    fi

    if [ -n "$action" ]; then
        local multi
        multi=$(map:action:field "$action" "$source" multi)
        if [ "$multi" == "true" ]; then
            rofi_args+=" -multi-select"
        fi
    fi

    echo -e "ROFI_ARGS: $rofi_args" >> "$MENU_LOG_FILE"

    echo -e "$rofi_args"
}
