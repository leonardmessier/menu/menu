#!/usr/bin/env bash

show:rasi:theme() {
    local THEME=$1
    local SCRIPT=$2

    local BASE_USER_THEMES_PATH="$MENU_LOCAL_SHARE/themes"
    local USER_SCRIPT_THEME_PATH="$BASE_USER_THEMES_PATH/$SCRIPT/$THEME.rasi"
    echo "USER_SCRIPT_THEME_PATH: $USER_SCRIPT_THEME_PATH" >> "$MENU_LOG_FILE"
    if [ -f "$USER_SCRIPT_THEME_PATH" ]; then
        echo "$USER_SCRIPT_THEME_PATH"
        return 0
    fi
    echo "User theme $USER_SCRIPT_THEME_PATH does not exist" >> "$MENU_LOG_FILE"

    local BASE_RASI_SHOW_PATH="$_MENU_ROOT/share/themes/rofi/show"
    local BUILT_IN_THEME_PATH="$BASE_RASI_SHOW_PATH/colorschemes/$THEME.rasi"
    echo "BUILT_IN_THEME_PATH: $BUILT_IN_THEME_PATH" >> "$MENU_LOG_FILE"
    if [ -f "$BUILT_IN_THEME_PATH" ]; then
        echo "$BUILT_IN_THEME_PATH"
        return 0
    fi
    echo "Built-in theme $BUILT_IN_THEME_PATH does not exist" >> "$MENU_LOG_FILE"

    return 1
}
