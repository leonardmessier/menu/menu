#!/usr/bin/env bash

show:rasi:layout() {
    local LAYOUT=$1

    local BASE_RASI_SHOW_PATH="$_MENU_ROOT/share/themes/rofi/show"
    local BUILT_IN_LAYOUT_PATH="$BASE_RASI_SHOW_PATH/layouts/$LAYOUT.rasi"
    echo "BUILT_IN_LAYOUT_PATH: $BUILT_IN_LAYOUT_PATH" >> "$MENU_LOG_FILE"
    if [ -f "$BUILT_IN_LAYOUT_PATH" ]; then
        echo "$BUILT_IN_LAYOUT_PATH"
        return 0
    fi
    echo "Built-in layout $BUILT_IN_LAYOUT_PATH does not exist" >> "$MENU_LOG_FILE"

    return 1
}
