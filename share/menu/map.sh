#!/bin/bash

map:field() {
    local SOURCE=$1
    local FIELD_NAME=$2
    local DELIMITER=:

    declare -A DEFINITION_FIELDS_MAP
    DEFINITION_FIELDS_MAP['icon']=1
    DEFINITION_FIELDS_MAP['name']=2
    DEFINITION_FIELDS_MAP['theme']=3
    DEFINITION_FIELDS_MAP['key']=4
    DEFINITION_FIELDS_MAP['binding']=5
    DEFINITION_FIELDS_MAP['default_action']=6
    DEFINITION_FIELDS_MAP['interpreter']=7
    DEFINITION_FIELDS_MAP['separator']=8
    DEFINITION_FIELDS_MAP['lines_count']=9
    DEFINITION_FIELDS_MAP['multi']=10
    DEFINITION_FIELDS_MAP['actions_list']=11
    DEFINITION_FIELDS_MAP['actions_bindings']=12
    DEFINITION_FIELDS_MAP['exit_code']=13

    FIELD="${DEFINITION_FIELDS_MAP[$FIELD_NAME]}"
    VALUE=$(echo -e "$SOURCE" | cut -d"${DELIMITER}" -f"${FIELD}")

    {
      echo -e "SOURCE: $SOURCE"
      echo -e "FIELD_NAME: $FIELD_NAME"
      echo -e "FIELD: $FIELD"
      echo -e "VALUE: $VALUE"
    } >> "$MENU_LOG_FILE"

    if [ -z "$VALUE" ]; then
        return 1
    fi

    echo -e "$VALUE"
}

map:action:field() {
    local ACTION=$1
    local SOURCE=$2
    local FIELD_NAME=$3

    local ACTIONS_LIST
    ACTIONS_LIST="$(map:field "$SOURCE" 'actions_list')"

    OLD_IFS=$IFS
    IFS=','
    local CHOSEN_ACTION
    for ACTION_ITEM in $ACTIONS_LIST; do
        N=$(echo -e "$ACTION_ITEM" | cut -d "/" -f2)
        if [ "$N" == "$ACTION" ]; then
            CHOSEN_ACTION=$ACTION_ITEM
            break
        fi
    done
    IFS=$OLD_IFS

    declare -A ACTION_FIELDS_MAP
    ACTION_FIELDS_MAP['exit_code']=1
    ACTION_FIELDS_MAP['name']=2
    ACTION_FIELDS_MAP['theme']=3
    ACTION_FIELDS_MAP['lines']=4
    ACTION_FIELDS_MAP['multi']=5

    FIELD="${ACTION_FIELDS_MAP[$FIELD_NAME]}"
    VALUE=$(echo -e "$CHOSEN_ACTION" | cut -d"/" -f"${FIELD}")

    {
      echo -e "FIELD_NAME: $FIELD_NAME"
      echo -e "FIELD: $FIELD"
      echo -e "VALUE: $VALUE"
    } >> "$MENU_LOG_FILE"

    if [ -z "$VALUE" ]; then
        echo "Missing value"
        return 1
    fi

    echo -e "$VALUE"
}


