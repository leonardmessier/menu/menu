#!/bin/bash

# shellcheck disable=SC1090
source "$_MENU_ROOT/share/menu/map.sh"
source "$_MENU_ROOT/share/menu/scripts.sh"

declare -ir ERROR_PARSE_OPTS=1
declare -ir ERROR_MISSING_SCRIPT=2
declare -ir ERROR_INVALID_SCRIPT=3
declare -ir ERROR_MISSING_FIELD_OR_ACTION_FIELD=4
declare -ir ERROR_BOTH_FIELD_AND_ACTION_FIELD=5
declare -ir ERROR_READ=6

# shellcheck disable=SC1090
if ! OPTS=$(getopt -o vhns: --long script:,field:,action:,action-field:, -n 'parse-options' -- "$@"); then
    echo -e "Failed parsing options." >&2
    # shellcheck disable=SC2086
    return $ERROR_PARSE_OPTS
fi

eval set -- "$OPTS"

SCRIPT_NAME=
FIELD=
ACTION=
ACTION_FIELD=

while true; do
    case "$1" in
        --script ) SCRIPT_NAME=$2; shift; shift ;;
        --field ) FIELD=$2; shift; shift ;;
        --action ) ACTION=$2; shift; shift ;;
        --action-field ) ACTION_FIELD=$2; shift; shift ;;
        * ) break;;
    esac
done

declare SCRIPTS_DIR
if [ -z "$MENU_SCRIPTS_DIR" ]; then
    SCRIPTS_DIR="$HOME/.local/share/menu/scripts"
else
    SCRIPTS_DIR="$MENU_SCRIPTS_DIR"
fi

if [ -z "$SCRIPT_NAME" ]; then
    echo -e "Missing --script option" >&2
    exit $ERROR_MISSING_SCRIPT
fi

if [ ! -f "$SCRIPTS_DIR/$SCRIPT_NAME" ]; then
    echo -n "No script $SCRIPT_NAME in $SCRIPTS_DIR"
    exit $ERROR_INVALID_SCRIPT
fi
SCRIPT="$SCRIPTS_DIR/$SCRIPT_NAME"

if [ -z "$FIELD" ] && [ -z "$ACTION_FIELD" ]; then
    echo -e "Missing --field or --action-field option" >&2
    exit $ERROR_MISSING_FIELD_OR_ACTION_FIELD
fi

if [ -n "$FIELD" ] && [ -n "$ACTION_FIELD" ]; then
    echo -e "--field and --action-field options cannot be used together" >&2
    exit $ERROR_BOTH_FIELD_AND_ACTION_FIELD
fi

if [ -n "$ACTION_FIELD" ] && [ -z "$ACTION" ]; then
    echo -e "--action-field option needs --action" >&2
fi

if ! READ_ONE="$(scripts:read:one "$SCRIPT")"; then
    echo -e "Could not read script $SCRIPT metadata"
    exit $ERROR_READ
fi

METADATA="$(echo -e "$READ_ONE" | cut -d$'\n' -f1)"

if [ -n "$FIELD" ]; then
    map:field "$METADATA" "$FIELD"
elif [ -n "$ACTION_FIELD" ]; then
    map:action:field "$ACTION" "$METADATA" "$ACTION_FIELD"
fi
