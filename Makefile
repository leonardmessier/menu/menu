COMMANDS := $(MAKEFILE_LIST)

ifneq ("$(wildcard .env)","")
		include .env
		export
endif

all: lint test

lint:
	@echo "→ \033[1mLINTING WITH SHELLCHECK\033[0m"
	@docker-compose -f .docker/docker-compose.yml run --rm shellcheck libexec/* share/menu/show/* share/menu/functions install.sh
	@echo "\033[32m✓ Success !\033[0m"

test: test-unit

test-unit:
	@echo "→ \033[1mUNIT TESTING WITH BATS\033[0m"
	@docker-compose -f .docker/docker-compose.yml run --rm bats -r /code/tests/unit
	@echo "\033[32m✓ Success !\033[0m"

.DEFAULT: all
.PHONY: lint test test-unit





